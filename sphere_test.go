package main

import (
	"math"
	"testing"
)

func TestSphereDefaultTransformation(t *testing.T) {
	s := sphere()
	identity := NewMatrix(4, 4)
	identity.Load([][]float64{
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1},
	})

	if !s.transform.Equals(identity) {
		t.Errorf("Expected sphere's default transform to be identity matrix, got %+v\n", s.transform)
	}
}

func TestChangingSphereTransformation(t *testing.T) {
	s := sphere()
	trans := translation(2, 3, 4)
	s.transform = trans
	if !s.transform.Equals(trans) {
		t.Errorf("Expected sphere's transform to be %+v, got %+v\n", trans, s.transform)
	}
}

func TestIntersectScaledSphere(t *testing.T) {
	r := ray(point(0, 0, -5), vector(0, 0, 1))
	s := sphere()
	s.transform = scaling(2, 2, 2)
	xs := r.intersect(s)

	if len(xs) != 2 {
		t.Errorf("Expected 2 intersections, got %d\n", len(xs))
	}
	if xs[0].t != 3 {
		t.Errorf("Expected xs[0].t to be 3, got %f\n", xs[0].t)
	}
	if xs[1].t != 7 {
		t.Errorf("Expected xs[1].t to be 7, got %f\n", xs[1].t)
	}
}

func TestIntersectTranslatedSphere(t *testing.T) {
	r := ray(point(0, 0, -5), vector(0, 0, 1))
	s := sphere()
	s.transform = translation(5, 0, 0)
	xs := r.intersect(s)

	if len(xs) != 0 {
		t.Errorf("Expected 0 intersections, got %d\n", len(xs))
	}
}

func TestSphereSurfaceNormal(t *testing.T) {
	s := sphere()
	// x axis
	n := s.normalAt(point(1, 0, 0))
	expected := vector(1, 0, 0)
	if !n.Equals(expected) {
		t.Errorf("Expected surface normal to be %+v, got %+v\n", expected, n)
	}

	// y axis
	n = s.normalAt(point(0, 1, 0))
	expected = vector(0, 1, 0)
	if !n.Equals(expected) {
		t.Errorf("Expected surface normal to be %+v, got %+v\n", expected, n)
	}

	// z axis
	n = s.normalAt(point(0, 0, 1))
	expected = vector(0, 0, 1)
	if !n.Equals(expected) {
		t.Errorf("Expected surface normal to be %+v, got %+v\n", expected, n)
	}

	// nonaxial point
	p := math.Sqrt(3) / 3
	n = s.normalAt(point(p, p, p))
	expected = vector(p, p, p)
	if !n.Equals(expected) {
		t.Errorf("Expected surface normal to be %+v, got %+v\n", expected, n)
	}

	expected = n.normalize()
	if !n.Equals(expected) {
		t.Errorf("Expected surface normal to be %+v, got %+v\n", expected, n)
	}
}

func TestSphereSurfaceNormalWithTranslation(t *testing.T) {
	s := sphere()
	s.transform = translation(0, 1, 0)
	n := s.normalAt(point(0, 1.70711, -.70711))
	expected := vector(0, 0.70711, -.70711)
	if !n.Equals(expected) {
		t.Errorf("Expected surface normal to be %+v, got %+v\n", expected, n)
	}
}

func TestSphereSurfaceNormalWithTransformation(t *testing.T) {
	s := sphere()
	s.transform = scaling(1, .5, 1).Multiply(rotationZ(math.Pi / 5))
	n := s.normalAt(point(0, math.Sqrt(2)/2, -math.Sqrt(2)/2))
	expected := vector(0, .97014, -.24254)
	if !n.Equals(expected) {
		t.Errorf("Expected surface normal to be %+v, got %+v\n", expected, n)
	}
}

func TestSphereEquality(t *testing.T) {
	s1 := sphere()
	s2 := sphere()
	if !s1.equals(s2) {
		t.Error("Expected default spheres to be equal")
	}

	s1.transform = translation(0.1, 1, -0.1)
	if s1.equals(s2) {
		t.Errorf("Expected spheres with different translations to be not equal: %+v and %+v\n", s1, s2)
	}

	s1 = sphere()
	s1.material = Material{
		color:    color(0.0, 0.1, 0.2),
		diffuse:  0.75,
		specular: 0.667,
	}
	if s1.equals(s2) {
		t.Errorf("Expected spheres with different materials to be not equal: %+v and %+v\n", s1, s2)
	}
}
