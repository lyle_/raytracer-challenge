package main

import (
	"math"
)

// Ray is a line with an origin and a direction
type Ray struct {
	origin, direction Tuple
}

func ray(origin Tuple, direction Tuple) Ray {
	return Ray{origin: origin, direction: direction}
}

func (r *Ray) position(t float64) Tuple {
	return r.origin.Add(r.direction.Multiply(vector(t, t, t)))
}

func (r *Ray) intersect(s Sphere) []Intersection {
	// Apply the sphere's inverse transformation to the ray to
	// allow continuing to assume that the sphere is a unit sphere at origin.
	ray := r.transform(s.transform.inverse())

	// Compute the discriminant; i.e., whether the ray intersects the sphere
	sphereToRay := ray.origin.Subtract(point(0, 0, 0))
	a := ray.direction.Dot(ray.direction)
	b := 2 * ray.direction.Dot(sphereToRay)
	c := sphereToRay.Dot(sphereToRay) - 1
	discriminant := (b * b) - 4*a*c
	// If the discriminant is negative, the ray misses the sphere
	if discriminant < 0 {
		return make([]Intersection, 0)
	}

	intersections := make([]Intersection, 2)
	intersections[0].t = (-b - math.Sqrt(discriminant)) / (2 * a)
	intersections[0].object = s
	intersections[1].t = (-b + math.Sqrt(discriminant)) / (2 * a)
	intersections[1].object = s
	return intersections
}

func (r *Ray) transform(matrix Matrix) Ray {
	return Ray{
		matrix.MultiplyTupler(r.origin),
		matrix.MultiplyTupler(r.direction),
	}
}
