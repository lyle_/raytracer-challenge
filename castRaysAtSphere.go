package main

import (
	"fmt"
	"time"
)

func castRaysAtSphere() {
	fmt.Println("Generating sphere image")
	start := time.Now()
	canvasSize := 500
	canvas := NewCanvas(canvasSize, canvasSize)

	// We assume a unit sphere at the origin
	shape := sphere()
	shape.material.color = color(1, 0.2, 1)

	// The point the rays are cast from
	eye := point(0, 0, -10)
	// Define a white light behind, above, and to the left of the eye
	light := PointLight{
		position:  point(-10, 10, -10),
		intensity: color(1, 1, 1),
	}

	// The wall is 7x7 in size and 10 units behind the sphere
	wallZ := 10.0
	wallSize := 7.0
	half := wallSize / 2
	// A single pixel is this many world space units
	pixelSize := wallSize / float64(canvasSize)

	// for each row of pixels on the canvas
	for y := 0; y < canvasSize; y++ {
		// compute the world y coordinate
		worldY := half - (pixelSize * float64(y))
		// for each pixel in the row
		for x := 0; x < canvasSize; x++ {
			// compute the world x coordinate
			worldX := -half + (pixelSize * float64(x))
			// the point on the wall that the ray will target
			target := point(worldX, worldY, wallZ)

			// cast a ray from the eye to the target on the wall
			ray := ray(eye, target.Subtract(eye).normalize())
			intersection, hit := hit(ray.intersect(shape))
			if hit {
				intersectionPoint := ray.position(intersection.t)
				// perpendicular to the surface at the intersection
				surfaceNormal := intersection.object.normalAt(intersectionPoint)
				// from the intersection back to the source of the ray
				eyeVector := ray.direction.negate()
				// calculate the color at this location
				color := lighting(intersection.object.material, light, intersectionPoint, eyeVector, surfaceNormal)
				canvas.SetColor(x, y, color)
			}
		}
	}

	canvas.Save("./sphere.ppm")
	elapsed := time.Since(start)
	fmt.Printf("Took %s\n", elapsed)
}
