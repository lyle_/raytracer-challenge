package main

import (
	"math"
	"testing"
)

func TestPoint(t *testing.T) {
	tuple := Tuple{0, 0, 0, 1}
	if !tuple.IsPoint() {
		t.Error(`A tuple with w=1 is a point`)
	}
	if tuple.IsVector() {
		t.Error(`A tuple with w=1 is not a vector`)
	}
}

func TestVector(t *testing.T) {
	tuple := Tuple{0, 0, 0, 0}
	if !tuple.IsVector() {
		t.Error(`A tuple with w=1 is a vector`)
	}
	if tuple.IsPoint() {
		t.Error(`A tuple with w=1 is not a point`)
	}
}

func TestPointFactory(t *testing.T) {
	point := point(1, 2, 3)
	if !point.IsPoint() {
		t.Error(`point() factory returns a point`)
	}
	if point.IsVector() {
		t.Error(`point() factory returns a point`)
	}
	tuple := Tuple{1, 2, 3, 1}
	if point != tuple {
		t.Error(`point() factory equality`)
	}
}

func TestVectorFactory(t *testing.T) {
	vector := vector(1, 2, 3)
	if !vector.IsVector() {
		t.Error(`vector() factory returns a vector`)
	}
	if vector.IsPoint() {
		t.Error(`vector() factory returns a vector`)
	}
	tuple := Tuple{1, 2, 3, 0}
	if vector != tuple {
		t.Error(`vector() factory equality`)
	}
}

func TestEquals(t *testing.T) {
	a := Tuple{0, 0, 0, 0}
	b := Tuple{0, 0, 0, 0}
	if !a.Equals(b) {
		t.Error(`Tuple equality`)
	}

	b.x = 1
	if a.Equals(b) {
		t.Error(`Tuple equality`)
	}
}

func TestAddition(t *testing.T) {
	a1 := Tuple{3, -2, 5, 1}
	a2 := Tuple{-2, 3, 1, 0}
	b := a1.Add(&a2)
	if !b.Equals(Tuple{1, 1, 6, 1}) {
		t.Error("Tuple addition, got ", b)
	}
}

func TestPointSubtraction(t *testing.T) {
	a1 := point(3, 2, 1)
	a2 := point(5, 6, 7)
	b := a1.Subtract(&a2)
	if !b.Equals(vector(-2, -4, -6)) {
		t.Error("Subtracting two points yields a vector, got ", b)
	}
}

func TestSubtractVectorFromPoint(t *testing.T) {
	p := point(3, 2, 1)
	v := vector(5, 6, 7)
	result := p.Subtract(&v)
	if !result.Equals(point(-2, -4, -6)) {
		t.Error("Subtracting a vector from a point yields a point, got ", result)
	}
}

func TestNegation(t *testing.T) {
	a := Tuple{1, -2, 3, -4}
	if !a.negate().Equals(Tuple{-1, 2, -3, 4}) {
		t.Error("Tuple negation, got ", a.negate())
	}
}

func TestMultiplicationByScalar(t *testing.T) {
	a := Tuple{1, -2, 3, -4}
	result := a.multiply(3.5)
	if !result.Equals(Tuple{3.5, -7, 10.5, -14}) {
		t.Error("Tuple multiplication by scalar, got ", result)
	}

	result2 := a.multiply(0.5)
	if !result2.Equals(Tuple{0.5, -1, 1.5, -2}) {
		t.Error("Tuple multiplication by scalar, got ", result2)
	}
}

func TestMultiplication(t *testing.T) {
	a := Tuple{1, .2, .4, 0}
	b := Tuple{.9, 1, .1, 0}
	result := a.Multiply(b)
	if !result.Equals(Tuple{.9, .2, .04, 0}) {
		t.Error("Tuple multiplication, got ", result)
	}
}

func TestDivision(t *testing.T) {
	a := Tuple{1, -2, 3, -4}
	result := a.divideBy(2)
	if !result.Equals(Tuple{0.5, -1, 1.5, -2}) {
		t.Error("Tuple division, got ", result)
	}
}

func TestVectorMagnitude(t *testing.T) {
	v1 := vector(1, 0, 0)
	if v1.magnitude() != 1 {
		t.Error("Vector magnitude, got ", v1.magnitude())
	}

	v2 := vector(0, 1, 0)
	if v2.magnitude() != 1 {
		t.Error("Vector magnitude, got ", v2.magnitude())
	}

	v3 := vector(0, 0, 1)
	if v3.magnitude() != 1 {
		t.Error("Vector magnitude, got ", v3.magnitude())
	}

	v4 := vector(1, 2, 3)
	if v4.magnitude() != math.Sqrt(14) {
		t.Error("Vector magnitude, got ", v4.magnitude())
	}

	v5 := vector(-1, -2, -3)
	if v5.magnitude() != math.Sqrt(14) {
		t.Error("Vector magnitude, got ", v5.magnitude())
	}
}

func TestVectorNormalization(t *testing.T) {
	v1 := vector(4, 0, 0)
	if !v1.normalize().Equals(vector(1, 0, 0)) {
		t.Error("vector normalization, got ", v1.normalize())
	}

	v2 := vector(1, 2, 3)
	m := v2.magnitude()
	if !v2.normalize().Equals(vector(0.26726, 0.53452, 0.80178)) ||
		!v2.normalize().Equals(vector(v2.x/m, v2.y/m, v2.z/m)) {
		t.Error("vector normalization, got ", v2.normalize())
	}

	v3 := vector(1, 2, 3)
	norm := v3.normalize()
	if norm.magnitude() != 1 {
		t.Error("magnitude of a normalized vector, got ", norm.magnitude())
	}
}

func TestTupleDotProduct(t *testing.T) {
	v1 := vector(1, 2, 3)
	v2 := vector(2, 3, 4)
	if v1.Dot(v2) != 20 || v2.Dot(v1) != 20 {
		t.Error("vector dot products, got ", v1.Dot(v2))
	}
}

func TestTupleCrossProduct(t *testing.T) {
	v1 := vector(1, 2, 3)
	v2 := vector(2, 3, 4)
	if !v1.cross(v2).Equals(vector(-1, 2, -1)) ||
		!v2.cross(v1).Equals(vector(1, -2, 1)) {
		t.Error("vector cross products, got ", v1.cross(v2))
	}
}

func TestTupleReflection(t *testing.T) {
	// Reflecting a vector approaching at 45 degrees
	v := vector(1, -1, 0)
	n := vector(0, 1, 0)
	reflected := v.reflect(n)
	expected := vector(1, 1, 0)
	if !reflected.Equals(expected) {
		t.Errorf("Vector reflection expected %+v, got %+v\n", expected, reflected)
	}

	// Reflecting a vector off a slanted surface
	v = vector(0, -1, 0)
	n = vector(math.Sqrt(2)/2, math.Sqrt(2)/2, 0)
	reflected = v.reflect(n)
	expected = vector(1, 0, 0)
	if !reflected.Equals(expected) {
		t.Errorf("Vector reflection expected %+v, got %+v\n", expected, reflected)
	}
}
