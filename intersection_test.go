package main

import (
	"testing"
)

func TestIntersection(t *testing.T) {
	s := sphere()
	i := Intersection{3.5, s}
	if i.t != 3.5 {
		t.Error("Expected t to be 3.5, got", i.t)
	}
	if !i.object.equals(s) {
		t.Errorf("Expected object to be %+v, got %+v\n", s, i.object)
	}
}

func TestIntersections(t *testing.T) {
	s := sphere()
	i1 := Intersection{1, s}
	i2 := Intersection{2, s}
	xs := intersections(i1, i2)
	if len(xs) != 2 {
		t.Error("Expected xs to be of size 2, got", len(xs))
	}
	if !xs[0].equals(i1) {
		t.Errorf("Expected xs[0] to be %+v, got %+v\n", i1, xs[0])
	}
	if xs[0].t != 1 {
		t.Errorf("Expected xs[0].t to be 1, got %+v\n", xs[0].t)
	}
	if !xs[1].equals(i2) {
		t.Errorf("Expected xs[1] to be %+v, got %+v\n", i2, xs[1])
	}
	if xs[1].t != 2 {
		t.Errorf("Expected xs[1].t to be 2, got %+v\n", xs[1].t)
	}
}

func TestIntersectionObject(t *testing.T) {
	r := ray(point(0, 0, -5), vector(0, 0, 1))
	s := sphere()
	xs := r.intersect(s)
	if len(xs) != 2 {
		t.Error("Expected 2 intersections, got", len(xs))
	}
	if !xs[0].object.equals(s) {
		t.Errorf("Expected xs[0].object to be %+v, got %+v\n", s, xs[0].object)
	}
	if !xs[1].object.equals(s) {
		t.Errorf("Expected xs[1].object to be %+v, got %+v\n", s, xs[1].object)
	}
}

func TestHitPositiveT(t *testing.T) {
	s := sphere()
	i1 := Intersection{1, s}
	i2 := Intersection{2, s}
	xs := intersections(i2, i1)
	i, _ := hit(xs)
	if !i.equals(i1) {
		t.Errorf("Expected i to be %+v, got %+v\n", i1, i)
	}
}

func TestHitSomeNegativeT(t *testing.T) {
	s := sphere()
	i1 := Intersection{-1, s}
	i2 := Intersection{1, s}
	xs := intersections(i2, i1)
	i, _ := hit(xs)
	if !i.equals(i2) {
		t.Errorf("Expected i to be %+v, got %+v\n", i2, i)
	}
}

func TestHitAllNegativeT(t *testing.T) {
	s := sphere()
	i1 := Intersection{-2, s}
	i2 := Intersection{-1, s}
	xs := intersections(i2, i1)
	i, exists := hit(xs)
	if exists {
		t.Errorf("Expected i to be nil, got %+v\n", i)
	}
}

func TestHitAlwaysLowestNonnegativeIntersection(t *testing.T) {
	s := sphere()
	i1 := Intersection{5, s}
	i2 := Intersection{7, s}
	i3 := Intersection{-3, s}
	i4 := Intersection{2, s}
	xs := intersections(i1, i2, i3, i4)
	i, _ := hit(xs)
	if !i.equals(i4) {
		t.Errorf("Expected i to be %+v, got %+v\n", i4, i)
	}
}

func TestPrepareComputations(t *testing.T) {
	// Precomputing the state of an intersection
	r := ray(point(0, 0, -5), vector(0, 0, 1))
	shape := sphere()
	i := Intersection{t: 4, object: shape}

	comps := i.prepareComputations(r)

	if comps.t != i.t {
		t.Errorf("Expected comps.t be %+v, got %+v\n", i.t, comps.t)
	}
	if !comps.object.equals(i.object) {
		t.Errorf("Expected comps.object be %+v, got %+v\n", i.object, comps.object)
	}
	expectedPoint := point(0, 0, -1)
	if !comps.point.Equals(expectedPoint) {
		t.Errorf("Expected comps.point be %+v, got %+v\n", expectedPoint, comps.point)
	}
	expectedEyeVector := vector(0, 0, -1)
	if !comps.eyeVector.Equals(expectedEyeVector) {
		t.Errorf("Expected comps.eyeVector be %+v, got %+v\n", expectedEyeVector, comps.eyeVector)
	}
	expectedNormalVector := vector(0, 0, -1)
	if !comps.normalVector.Equals(expectedNormalVector) {
		t.Errorf("Expected comps.normalVector be %+v, got %+v\n", expectedNormalVector, comps.normalVector)
	}
}
