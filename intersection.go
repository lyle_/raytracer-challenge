package main

import "sort"

// Intersection describes the time value and object being intersected by a Ray.
type Intersection struct {
	t      float64
	object Sphere
}

// Return a new collection of the given intersection objects ordered by t
func intersections(intersections ...Intersection) []Intersection {
	sort.Slice(intersections, func(i, j int) bool {
		return intersections[i].t < intersections[j].t
	})
	return intersections
}

// Returns the single visible hit among the intersections, if any
func hit(intersections []Intersection) (Intersection, bool) {
	// We assume intersections is sorted by t, ascending
	for _, intersection := range intersections {
		if intersection.t > 0 {
			return intersection, true
		}
	}
	return Intersection{}, false
}

func (i *Intersection) equals(o Intersection) bool {
	return i.t == o.t && i.object.equals(o.object)
}

func (i *Intersection) prepareComputations(ray Ray) IntersectionComputations {
	comps := IntersectionComputations{}
	comps.t = i.t
	comps.object = i.object
	comps.point = ray.position(i.t)
	comps.eyeVector = ray.direction.negate()
	comps.normalVector = comps.object.normalAt(comps.point)

	return comps
}

// IntersectionComputations encapsulates some precomputed computations
// about a ray intersection.
type IntersectionComputations struct {
	Intersection
	point, eyeVector, normalVector Tuple
}
