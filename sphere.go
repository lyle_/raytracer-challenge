package main

// Sphere is a round surface with every point equidistant from its center
type Sphere struct {
	transform Matrix
	material  Material
}

func sphere() Sphere {
	identity := NewMatrix(4, 4)
	identity.Load([][]float64{
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1},
	})
	return Sphere{identity, defaultMaterial()}
}

// Equals returns whether the provided sphere is equal to this one
func (s *Sphere) equals(o Sphere) bool {
	return s.transform.Equals(o.transform) &&
		s.material.Equals(o.material)
}

// normalAt returns the surface normal at the given point
func (s *Sphere) normalAt(worldPoint Tupler) Tuple {
	objectPoint := s.transform.inverse().MultiplyTupler(worldPoint)
	objectNormal := objectPoint.Subtract(point(0, 0, 0))
	worldNormal := s.transform.inverse().Transpose().MultiplyTupler(objectNormal)
	return Tuple{
		worldNormal.x,
		worldNormal.y,
		worldNormal.z,
		0}.normalize()
}
