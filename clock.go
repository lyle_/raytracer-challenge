package main

import (
	"fmt"
	"math"
)

func drawClock() {
	canvas := NewCanvas(width, height)
	clockRadius := float64(width) * .75
	twelve := point(0, 0, 0).Subtract(vector(0, clockRadius/2, 0))
	canvas.SetColorAtPoint(toCanvas(twelve), color(1, 1, 1))

	// Draw a point for each hour on a clock face
	for i := 1.0; i < 12; i++ {
		// The canvas is oriented along the z-axis
		rotation := rotationZ(float64(i) * (math.Pi / 6))
		hour := rotation.MultiplyTupler(twelve)
		colorValue := (i + 3) * (1.0 / 12)
		canvas.SetColorAtPoint(toCanvas(hour), color(colorValue, 0, 0))
		fmt.Printf("Hour %f, color %f, %+v\n", i, colorValue, hour)
	}
	canvas.Save("./clock.ppm")
}

// Translate the point from world coordinates to canvas coordinates
func toCanvas(p Tupler) Tupler {
	return p.Add(point(width/2, height/2, 0))
}
