package main

import (
	"math"
	"testing"
)

func TestMatrixConstruction(t *testing.T) {
	m := NewMatrix(4, 4)
	if m.width != 4 || m.height != 4 {
		t.Error("Matrix expected to be 4x4, got ", m)
	}

	for row := 0; row < m.height; row++ {
		for col := 0; col < m.width; col++ {
			value := m.At(row, col)
			if value != 0 {
				t.Errorf("Expected elements to be initialized to zero; got %f at %dx%d", value, row, col)
			}
		}
	}

	m.Load([][]float64{
		{1, 2, 3, 4},
		{5.5, 6.5, 7.5, 8.5},
		{9, 10, 11, 12},
		{13.5, 14.5, 15.5, 16.5},
	})

	if m.elements[0][0] != 1 {
		t.Error("A) Expected 1 at [0,0], got:", m.At(0, 0))
	}
	if m.At(0, 0) != 1 {
		t.Error("B) Expected 1 at [0,0], got:", m.At(0, 0))
	}
	if m.At(0, 3) != 4 {
		t.Error("Expected 4 at [0,3], got:", m.At(0, 3))
	}
	if m.At(1, 0) != 5.5 {
		t.Error("Expected 5.5 at [1,0], got:", m.At(1, 0))
	}
	if m.At(1, 2) != 7.5 {
		t.Error("Expected 7.5 at [1,2], got:", m.At(1, 2))
	}
	if m.At(2, 2) != 11 {
		t.Error("Expected 11 at [2,2], got:", m.At(2, 2))
	}
	if m.At(3, 0) != 13.5 {
		t.Error("Expected 13.5 at [3,0], got:", m.At(3, 0))
	}
	if m.At(3, 2) != 15.5 {
		t.Error("Expected 15.5 at [3,2], got:", m.At(3, 2))
	}
}

func Test2x2MatrixConstruction(t *testing.T) {
	m := NewMatrix(2, 2)
	m.Load([][]float64{
		{-3, 5},
		{1, -2},
	})
	if m.At(0, 0) != -3 {
		t.Error("Expected -3 at [0,0], got:", m.At(0, 0))
	}
	if m.At(0, 1) != 5 {
		t.Error("Expected 5 at [0,1], got:", m.At(0, 1))
	}
	if m.At(1, 0) != 1 {
		t.Error("Expected 1 at [1,0], got:", m.At(1, 0))
	}
	if m.At(1, 1) != -2 {
		t.Error("Expected -2 at [1,1], got:", m.At(1, 1))
	}
}

func TestMatrixComparison(t *testing.T) {
	m1 := NewMatrix(4, 4)
	m1.Load([][]float64{
		{1, 2, 3, 4},
		{5, 6, 7, 8},
		{9, 8, 7, 6},
		{5, 4, 3, 2},
	})
	m2 := NewMatrix(4, 4)
	m2.Load([][]float64{
		{1, 2, 3, 4},
		{5, 6, 7, 8},
		{9, 8, 7, 6},
		{5, 4, 3, 2},
	})

	if !m1.Equals(m2) {
		t.Error("Expected matrices to be equal")
	}

	m2.Set(3, 3, 100)
	if m1.Equals(m2) {
		t.Error("Expected matrices to be different")
	}
}

func TestSliceComparison(t *testing.T) {
	s1 := []float64{1, 2, 3, 4, 5, 6}
	s2 := []float64{1, 2, 3, 4, 5, 6}
	s3 := []float64{6, 5, 4, 3}
	if !slicesEqual(s1, s2) {
		t.Error("Expected slices to be equal", s1, s2)
	}
	if slicesEqual(s1, s3) {
		t.Error("Expected slices to be unequal", s1, s2)
	}
}

func TestRow(t *testing.T) {
	m1 := NewMatrix(2, 2)
	m1.Load([][]float64{
		{1, 2},
		{5, 6},
	})

	if !slicesEqual(m1.Row(0), []float64{1, 2}) {
		t.Error("Expected row 0 to be {1, 2}")
	}
	if !slicesEqual(m1.Row(1), []float64{5, 6}) {
		t.Error("Expected row 1 to be {5, 6}")
	}
}

func TestSetRow(t *testing.T) {
	matrix := NewMatrix(4, 4)
	matrix.Load([][]float64{
		{1, 2, 3, 4},
		{5, 6, 7, 8},
		{9, 8, 7, 6},
		{5, 4, 3, 2},
	})

	matrix.SetRow(1, []float64{0, 0, 0, 0})

	expected := NewMatrix(4, 4)
	expected.Load([][]float64{
		{1, 2, 3, 4},
		{0, 0, 0, 0},
		{9, 8, 7, 6},
		{5, 4, 3, 2},
	})

	if !matrix.Equals(expected) {
		t.Errorf("Expected matrix after SetRow to be %+v, got %+v", expected, matrix)
	}
}

func TestRowTuple(t *testing.T) {
	m1 := NewMatrix(4, 2)
	m1.Load([][]float64{
		{1, 2, 3, 4},
		{5, 6, 7, 8},
	})

	result := m1.RowTuple(0)
	expected := Tuple{1, 2, 3, 4}
	if !result.Equals(expected) {
		t.Errorf("Expected row 0 tuple to be %+v, got %+v", expected, result)
	}

	result = m1.RowTuple(1)
	expected = Tuple{5, 6, 7, 8}
	if !result.Equals(expected) {
		t.Errorf("Expected row 1 tuple to be %+v, got %+v", expected, result)
	}
}

func TestColumn(t *testing.T) {
	m1 := NewMatrix(2, 2)
	m1.Load([][]float64{
		{1, 2},
		{5, 6},
	})

	if !slicesEqual(m1.Column(0), []float64{1, 5}) {
		t.Error("Expected column 0 to be {1, 5}")
	}
	if !slicesEqual(m1.Column(1), []float64{2, 6}) {
		t.Error("Expected column 1 to be {2, 6}")
	}
}

func TestMatrixMultiplication(t *testing.T) {
	m1 := NewMatrix(4, 4)
	m1.Load([][]float64{
		{1, 2, 3, 4},
		{5, 6, 7, 8},
		{9, 8, 7, 6},
		{5, 4, 3, 2},
	})
	m2 := NewMatrix(4, 4)
	m2.Load([][]float64{
		{-2, 1, 2, 3},
		{3, 2, 1, -1},
		{4, 3, 6, 5},
		{1, 2, 7, 8},
	})
	expected := NewMatrix(4, 4)
	expected.Load([][]float64{
		{20, 22, 50, 48},
		{44, 54, 114, 108},
		{40, 58, 110, 102},
		{16, 26, 46, 42},
	})

	product := m1.Multiply(m2)
	if !product.Equals(expected) {
		t.Errorf("Expected matrix multiplication is %+v, got %+v", expected, product)
	}
}

func TestMatrixMultiplyByTuple(t *testing.T) {
	matrix := NewMatrix(4, 4)
	matrix.Load([][]float64{
		{1, 2, 3, 4},
		{2, 4, 4, 2},
		{8, 6, 4, 1},
		{0, 0, 0, 1},
	})
	tuple := Tuple{1, 2, 3, 1}
	expected := Tuple{18, 24, 33, 1}
	product := matrix.MultiplyTupler(tuple)
	if !product.Equals(expected) {
		t.Errorf("Expected matrix multiplication by tuple to be is %+v, got %+v", expected, product)
	}
}

func TestMatrixMutiplyByIdentity(t *testing.T) {
	matrix := NewMatrix(4, 4)
	matrix.Load([][]float64{
		{0, 1, 2, 4},
		{1, 2, 4, 8},
		{2, 4, 8, 16},
		{4, 8, 16, 32},
	})
	identity := NewMatrix(4, 4)
	identity.Load([][]float64{
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1},
	})

	result := matrix.Multiply(identity)
	if !result.Equals(matrix) {
		t.Errorf("Expected identity matrix multiplication is %+v, got %+v", matrix, result)
	}
}

func TestMatrixTranspose(t *testing.T) {
	matrix := NewMatrix(4, 4)
	matrix.Load([][]float64{
		{0, 9, 3, 0},
		{9, 8, 0, 8},
		{1, 8, 5, 3},
		{0, 0, 5, 8},
	})
	expected := NewMatrix(4, 4)
	expected.Load([][]float64{
		{0, 9, 1, 0},
		{9, 8, 8, 0},
		{3, 0, 5, 5},
		{0, 8, 3, 8},
	})

	result := matrix.Transpose()
	if !result.Equals(expected) {
		t.Errorf("Expected matrix transposition to be %+v, got %+v", expected, result)
	}
}

func TestMatrixTransposeIdenitty(t *testing.T) {
	identity := NewMatrix(4, 4)
	identity.Load([][]float64{
		{1, 0, 0, 0},
		{0, 1, 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1},
	})

	result := identity.Transpose()
	if !result.Equals(identity) {
		t.Errorf("Expected identity matrix transposition to be %+v, got %+v", identity, result)
	}
}

func TestMatrixDeterminant(t *testing.T) {
	matrix := NewMatrix(2, 2)
	matrix.Load([][]float64{
		{1, 5},
		{-3, 2},
	})
	var expected float64 = 17
	var result float64 = matrix.determinant()
	if result != expected {
		t.Errorf("Expected matrix determinant to be %+v, got %+v", expected, result)
	}
}

func TestSubmatrix(t *testing.T) {
	matrix := NewMatrix(3, 3)
	matrix.Load([][]float64{
		{1, 5, 0},
		{-3, 2, 7},
		{0, 6, -3},
	})

	result := matrix.Submatrix(0, 2)

	expected := NewMatrix(2, 2)
	expected.Load([][]float64{
		{-3, 2},
		{0, 6},
	})

	if !result.Equals(expected) {
		t.Errorf("Expected submatrix to be %+v, got %+v", expected, result)
	}
}

func TestMatrixMinor(t *testing.T) {
	matrix := NewMatrix(3, 3)
	matrix.Load([][]float64{
		{3, 5, 0},
		{2, -1, -7},
		{6, -1, 5},
	})

	submatrix := matrix.Submatrix(1, 0)
	determinant := submatrix.determinant()
	expected := 25.0
	if determinant != expected {
		t.Errorf("Expected matrix determinant to be %+v, got %+v", expected, determinant)
	}

	// The minor of an element at i, j is the determinant of the submatrix at i,j
	minor := matrix.minor(1, 0)
	if minor != expected {
		t.Errorf("Expected matrix minor to be %+v, got %+v", expected, minor)
	}
}

func TestMatrixCofactor(t *testing.T) {
	matrix := NewMatrix(3, 3)
	matrix.Load([][]float64{
		{3, 5, 0},
		{2, -1, -7},
		{6, -1, 5},
	})

	minor := matrix.minor(0, 0)
	expectedMinor := -12.0
	if minor != expectedMinor {
		t.Errorf("Expected matrix minor to be %+v, got %+v", expectedMinor, minor)
	}

	cofactor := matrix.cofactor(0, 0)
	if cofactor != expectedMinor {
		t.Errorf("Expected matrix cofactor to be %+v, got %+v", expectedMinor, cofactor)
	}

	minor = matrix.minor(1, 0)
	expectedMinor = 25.0
	if minor != expectedMinor {
		t.Errorf("Expected matrix minor to be %+v, got %+v", expectedMinor, minor)
	}

	cofactor = matrix.cofactor(1, 0)
	if cofactor != -expectedMinor {
		t.Errorf("Expected matrix cofactor to be %+v, got %+v", -expectedMinor, cofactor)
	}
}

func Test3x3MatrixCofactor(t *testing.T) {
	matrix := NewMatrix(3, 3)
	matrix.Load([][]float64{
		{1, 2, 6},
		{-5, 8, -4},
		{2, 6, 4},
	})

	cofactor := matrix.cofactor(0, 0)
	if cofactor != 56 {
		t.Errorf("Expected matrix cofactor to be %+v, got %+v", 56, cofactor)
	}
	cofactor = matrix.cofactor(0, 1)
	if cofactor != 12 {
		t.Errorf("Expected matrix cofactor to be %+v, got %+v", 12, cofactor)
	}
	cofactor = matrix.cofactor(0, 2)
	if cofactor != -46 {
		t.Errorf("Expected matrix cofactor to be %+v, got %+v", -46, cofactor)
	}
	determinant := matrix.determinant()
	if determinant != -196 {
		t.Errorf("Expected matrix determinant to be %+v, got %+v", -196, determinant)
	}
}

func Test4x4MatrixCofactor(t *testing.T) {
	matrix := NewMatrix(4, 4)
	matrix.Load([][]float64{
		{-2, -8, 3, 5},
		{-3, 1, 7, 3},
		{1, 2, -9, 6},
		{-6, 7, 7, -9},
	})

	cofactor := matrix.cofactor(0, 0)
	if cofactor != 690 {
		t.Errorf("Expected matrix cofactor to be %+v, got %+v", 690, cofactor)
	}
	cofactor = matrix.cofactor(0, 1)
	if cofactor != 447 {
		t.Errorf("Expected matrix cofactor to be %+v, got %+v", 447, cofactor)
	}
	cofactor = matrix.cofactor(0, 2)
	if cofactor != 210 {
		t.Errorf("Expected matrix cofactor to be %+v, got %+v", 210, cofactor)
	}
	cofactor = matrix.cofactor(0, 3)
	if cofactor != 51 {
		t.Errorf("Expected matrix cofactor to be %+v, got %+v", 51, cofactor)
	}
	determinant := matrix.determinant()
	if determinant != -4071 {
		t.Errorf("Expected matrix determinant to be %+v, got %+v", -4071, determinant)
	}
}

func TestMatrixInvertible(t *testing.T) {
	matrix := NewMatrix(4, 4)
	matrix.Load([][]float64{
		{6, 4, 4, 4},
		{5, 5, 7, 6},
		{4, -9, 3, -7},
		{9, 1, 7, -6},
	})
	determinant := matrix.determinant()
	if determinant != -2120 {
		t.Errorf("Expected matrix determinant to be %+v, got %+v", -2120, determinant)
	}
	if !matrix.invertible() {
		t.Error("Expected matrix to be invertible")
	}

	matrix.Load([][]float64{
		{-4, 2, -2, 3},
		{9, 6, 2, 6},
		{0, -5, 1, -5},
		{0, 0, 0, 0},
	})
	determinant = matrix.determinant()
	if determinant != 0 {
		t.Errorf("Expected matrix determinant to be %+v, got %+v", 0, determinant)
	}
	if matrix.invertible() {
		t.Error("Expected matrix to be non-invertible")
	}
}

func TestMatrixInverse(t *testing.T) {
	matrix := NewMatrix(4, 4)
	matrix.Load([][]float64{
		{-5, 2, 6, -8},
		{1, -5, 1, 8},
		{7, 7, -6, -7},
		{1, -3, 7, 4},
	})
	inverse := matrix.inverse()

	determinant := matrix.determinant()
	if determinant != 532 {
		t.Errorf("Expected matrix determinant to be %+v, got %+v", 532, determinant)
	}
	cofactor := matrix.cofactor(2, 3)
	if cofactor != -160 {
		t.Errorf("Expected matrix cofactor to be %+v, got %+v", -160, cofactor)
	}
	if !floatEqual(inverse.At(3, 2), -160.0/532) {
		t.Errorf("Expected inverse[3, 2] to be %+v, got %+v", float64(-160.0/532), inverse.At(3, 2))
	}
	cofactor = matrix.cofactor(3, 2)
	if cofactor != 105 {
		t.Errorf("Expected matrix cofactor to be %+v, got %+v", 105, cofactor)
	}
	if inverse.At(2, 3) != 105.0/532 {
		t.Errorf("Expected inverse[2, 3] to be %+v, got %+v", float64(105.0/532), inverse.At(2, 3))
	}

	expectedInverse := NewMatrix(4, 4)
	expectedInverse.Load([][]float64{
		{0.21805, 0.45113, 0.24060, -0.04511},
		{-0.80827, -1.45677, -0.44361, 0.52068},
		{-0.07895, -0.22368, -0.05263, 0.19737},
		{-0.52256, -0.81391, -0.30075, 0.30639},
	})
	if !inverse.Equals(expectedInverse) {
		t.Errorf("Expected inverse to be %+v, got %+v", expectedInverse, inverse)
	}

	matrix.Load([][]float64{
		{8, -5, 9, 2},
		{7, 5, 6, 1},
		{-6, 0, 9, 6},
		{-3, 0, -9, -4},
	})
	expectedInverse.Load([][]float64{
		{-0.15385, -0.15385, -0.28205, -0.53846},
		{-0.07692, 0.12308, 0.02564, 0.03077},
		{0.35897, 0.35897, 0.43590, 0.92308},
		{-0.69231, -0.69231, -0.76923, -1.92308},
	})
	inverse = matrix.inverse()
	if !inverse.Equals(expectedInverse) {
		t.Errorf("Expected inverse to be %+v, got %+v", expectedInverse, inverse)
	}

	matrix.Load([][]float64{
		{9, 3, 0, 9},
		{-5, -2, -6, -3},
		{-4, 9, 6, 4},
		{-7, 6, 6, 2},
	})
	expectedInverse.Load([][]float64{
		{-0.04074, -0.07778, 0.14444, -0.22222},
		{-0.07778, 0.03333, 0.36667, -0.33333},
		{-0.02901, -0.14630, -0.10926, 0.12963},
		{0.17778, 0.06667, -0.26667, 0.33333},
	})
	inverse = matrix.inverse()
	if !inverse.Equals(expectedInverse) {
		t.Errorf("Expected inverse to be %+v, got %+v", expectedInverse, inverse)
	}

	// Test inverting multiplication
	matrix.Load([][]float64{
		{3, -9, 7, 3},
		{3, -8, 2, -9},
		{-4, 4, 4, 1},
		{-6, 5, -1, 1},
	})
	b := NewMatrix(4, 4)
	b.Load([][]float64{
		{8, 2, 2, 2},
		{3, -1, 7, 0},
		{7, 0, 5, 4},
		{6, -2, 0, 5},
	})
	multiple := matrix.Multiply(b)
	original := multiple.Multiply(b.inverse())
	if !original.Equals(matrix) {
		t.Error("Expected the inverse of a multiple to be the same as the original")
	}
}

func TestMatrixTranslation(t *testing.T) {
	transform := translation(5, -3, 2)
	p := point(-3, 4, 5)
	transformed := transform.MultiplyTupler(p)
	expected := point(2, 1, 7)
	if !transformed.Equals(expected) {
		t.Errorf("Expected transformed point to be %+v, got %+v", expected, transformed)
	}

	// The inverse of a translation matrix moves points in reverse
	transform = translation(5, -3, 2)
	inverse := transform.inverse()
	p = point(-3, 4, 5)
	transformed = inverse.MultiplyTupler(p)
	expected = point(-8, 7, 3)
	if !transformed.Equals(expected) {
		t.Errorf("Expected transformed point to be %+v, got %+v", expected, transformed)
	}

	// Multiplying a translation matrix by a vector should not change the vector
	transform = translation(5, -3, 2)
	vector := vector(-3, 4, 5)
	transformed = transform.MultiplyTupler(vector)
	if !transformed.Equals(vector) {
		t.Errorf("Expected translated vector to be the same (%+v), got %+v", vector, transformed)
	}
}

func TestMatrixScaling(t *testing.T) {
	transform := scaling(2, 3, 4)
	p := point(-4, 6, 8)
	transformed := transform.MultiplyTupler(p)
	expected := point(-8, 18, 32)
	if !transformed.Equals(expected) {
		t.Errorf("Expected scaled point to be %+v, got %+v", expected, transformed)
	}

	// Unline translation, scaling does apply to vectors
	transform = scaling(2, 3, 4)
	v := vector(-4, 6, 8)
	transformed = transform.MultiplyTupler(v)
	expected = vector(-8, 18, 32)
	if !transformed.Equals(expected) {
		t.Errorf("Expected scaled vector to be %+v, got %+v", expected, transformed)
	}

	// Multiplying a tuple by the inverse of a scaling matrix will scale the tuple
	// in the opposite way.
	transform = scaling(2, 3, 4)
	inverse := transform.inverse()
	v = vector(-4, 6, 8)
	transformed = inverse.MultiplyTupler(v)
	expected = vector(-2, 2, 2)
	if !transformed.Equals(expected) {
		t.Errorf("Expected scaled vector to be %+v, got %+v", expected, transformed)
	}

	// Reflection is scaling by a negative value
	transform = scaling(-1, 1, 1)
	p = point(2, 3, 4)
	transformed = transform.MultiplyTupler(p)
	expected = point(-2, 3, 4)
	if !transformed.Equals(expected) {
		t.Errorf("Expected reflected point to be %+v, got %+v", expected, transformed)
	}
}

func TestMatrixRotationX(t *testing.T) {
	// Rotating a point about the x axis
	p := point(0, 1, 0)
	halfQuarter := rotationX(math.Pi / 4)
	fullQuarter := rotationX(math.Pi / 2)

	hq := halfQuarter.MultiplyTupler(p)
	hqExpected := point(0, math.Sqrt(2)/2, math.Sqrt(2)/2)
	if !hq.Equals(hqExpected) {
		t.Errorf("Expected half quarter of %+v, got %+v", hqExpected, hq)
	}
	fq := fullQuarter.MultiplyTupler(p)
	fqExpected := point(0, 0, 1)
	if !fq.Equals(fqExpected) {
		t.Errorf("Expected full quarter of %+v, got %+v", fqExpected, fq)
	}

	// The inverse of a rotation matrix rotates in the opposite direction
	p = point(0, 1, 0)
	inverseHalfQuarter := rotationX(math.Pi / 4).inverse()
	transformed := inverseHalfQuarter.MultiplyTupler(p)
	inverseHalfQuarterExpected := point(0, math.Sqrt(2)/2, -math.Sqrt(2)/2)
	if !transformed.Equals(inverseHalfQuarterExpected) {
		t.Errorf("Expected inverse half quarter of %+v, got %+v", inverseHalfQuarterExpected, transformed)
	}
}

func TestMatrixRotationY(t *testing.T) {
	// Rotating a point about the y axis
	p := point(0, 0, 1)
	halfQuarter := rotationY(math.Pi / 4)
	fullQuarter := rotationY(math.Pi / 2)

	hq := halfQuarter.MultiplyTupler(p)
	hqExpected := point(math.Sqrt(2)/2, 0, math.Sqrt(2)/2)
	if !hq.Equals(hqExpected) {
		t.Errorf("Expected half quarter of %+v, got %+v", hqExpected, hq)
	}
	fq := fullQuarter.MultiplyTupler(p)
	fqExpected := point(1, 0, 0)
	if !fq.Equals(fqExpected) {
		t.Errorf("Expected full quarter of %+v, got %+v", fqExpected, fq)
	}
}

func TestMatrixRotationZ(t *testing.T) {
	// Rotating a point about the z axis
	p := point(0, 1, 0)
	halfQuarter := rotationZ(math.Pi / 4)
	fullQuarter := rotationZ(math.Pi / 2)

	hq := halfQuarter.MultiplyTupler(p)
	hqExpected := point(-math.Sqrt(2)/2, math.Sqrt(2)/2, 0)
	if !hq.Equals(hqExpected) {
		t.Errorf("Expected half quarter of %+v, got %+v", hqExpected, hq)
	}
	fq := fullQuarter.MultiplyTupler(p)
	fqExpected := point(-1, 0, 0)
	if !fq.Equals(fqExpected) {
		t.Errorf("Expected full quarter of %+v, got %+v", fqExpected, fq)
	}
}

func TestMatrixShearing(t *testing.T) {
	// A shearing transformation moves x in proportion to y
	transform := shearing(1, 0, 0, 0, 0, 0)
	p := point(2, 3, 4)
	transformed := transform.MultiplyTupler(p)
	expected := point(5, 3, 4)
	if !transformed.Equals(expected) {
		t.Errorf("Expected sheared point of %+v, got %+v", expected, transformed)
	}

	// A shearing transformation moves x in proportion to z
	transform = shearing(0, 1, 0, 0, 0, 0)
	p = point(2, 3, 4)
	transformed = transform.MultiplyTupler(p)
	expected = point(6, 3, 4)
	if !transformed.Equals(expected) {
		t.Errorf("Expected sheared point of %+v, got %+v", expected, transformed)
	}

	// A shearing transformation moves y in proportion to x
	transform = shearing(0, 0, 1, 0, 0, 0)
	p = point(2, 3, 4)
	transformed = transform.MultiplyTupler(p)
	expected = point(2, 5, 4)
	if !transformed.Equals(expected) {
		t.Errorf("Expected sheared point of %+v, got %+v", expected, transformed)
	}

	// A shearing transformation moves y in proportion to z
	transform = shearing(0, 0, 0, 1, 0, 0)
	p = point(2, 3, 4)
	transformed = transform.MultiplyTupler(p)
	expected = point(2, 7, 4)
	if !transformed.Equals(expected) {
		t.Errorf("Expected sheared point of %+v, got %+v", expected, transformed)
	}

	// A shearing transformation moves z in proportion to x
	transform = shearing(0, 0, 0, 0, 1, 0)
	p = point(2, 3, 4)
	transformed = transform.MultiplyTupler(p)
	expected = point(2, 3, 6)
	if !transformed.Equals(expected) {
		t.Errorf("Expected sheared point of %+v, got %+v", expected, transformed)
	}

	// A shearing transformation moves z in proportion to y
	transform = shearing(0, 0, 0, 0, 0, 1)
	p = point(2, 3, 4)
	transformed = transform.MultiplyTupler(p)
	expected = point(2, 3, 7)
	if !transformed.Equals(expected) {
		t.Errorf("Expected sheared point of %+v, got %+v", expected, transformed)
	}
}

func TestChainingTransformations(t *testing.T) {
	// Individual transformations are applied in sequence
	p := point(1, 0, 1)
	a := rotationX(math.Pi / 2)
	b := scaling(5, 5, 5)
	c := translation(10, 5, 7)

	// Apply rotation first
	p2 := a.MultiplyTupler(p)
	expected := point(1, -1, 0)
	if !p2.Equals(expected) {
		t.Errorf("Expected transformed point of %+v, got %+v", expected, p2)
	}

	// Then apply scaling
	p3 := b.MultiplyTupler(p2)
	expected = point(5, -5, 0)
	if !p3.Equals(expected) {
		t.Errorf("Expected transformed point of %+v, got %+v", expected, p2)
	}

	// Then apply translation
	p4 := c.MultiplyTupler(p3)
	expected = point(15, 0, 7)
	if !p4.Equals(expected) {
		t.Errorf("Expected transformed point of %+v, got %+v", expected, p2)
	}

	// Chained transformations must be applied in reverse order
	transformation := c.Multiply(b).Multiply(a)
	result := transformation.MultiplyTupler(p)
	expected = point(15, 0, 7)
	if !result.Equals(expected) {
		t.Errorf("Expected transformed point of %+v, got %+v", expected, result)
	}
}
