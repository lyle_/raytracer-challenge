package main

import "testing"

func TestColorAsTuple(t *testing.T) {
	c := color(1, .5, 0)
	if c.red() != 1 || c.green() != .5 || c.blue() != 0 {
		t.Error("color as tuple, got ", c)
	}
}

func TestColorAddition(t *testing.T) {
	c1 := color(.9, .6, .75)
	c2 := color(.7, .1, .25)
	sum := c1.AddColor(c2)
	if !sum.Equals(color(1.6, .7, 1)) {
		t.Error("color addition, got ", c1.Add(c2))
	}
}

func TestColorSubtraction(t *testing.T) {
	c1 := color(.9, .6, .75)
	c2 := color(.7, .1, .25)
	difference := c1.SubtractColor(c2)
	if !difference.Equals(color(.2, .5, .5)) {
		t.Error("color subtraction, got ", difference)
	}
}

func TestColorMultiplicationByScalar(t *testing.T) {
	c1 := color(.2, .3, .4)
	multiple := c1.MultiplyColor(2)
	if !multiple.Equals(color(.4, .6, .8)) {
		t.Error("color multiplication, got ", multiple)
	}
}

func TestColorMultiplication(t *testing.T) {
	c1 := color(1, .2, .4)
	c2 := color(.9, 1, .1)
	multiple := c1.MultiplyColors(c2)
	if !multiple.Equals(color(.9, .2, .04)) {
		t.Error("color multiplication, got ", multiple)
	}
}
