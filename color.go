package main

// Color is a tuple of red, blue, and green values between 0 and 1.
type Color struct {
	Tuple
}

// W is a Tuple value which distinguishes between a point and a vector.
// We treat colors as vectors.
func W() float64 {
	return 0
}

// AddColor performs addition with tuple components of the
// provided colors and returns the sum.
func (c Color) AddColor(o Color) Color {
	t := c.Add(o)
	return color(t.x, t.y, t.z)
}

// SubtractColor performs subtraction with tuple components of the
// provided colors and returns the difference.
func (c Color) SubtractColor(o Color) Color {
	t := c.Subtract(o)
	return color(t.x, t.y, t.z)
}

// MultiplyColor performs scalar multiplication on the tuple components
// of the receiver and returns the product.
func (c Color) MultiplyColor(m float64) Color {
	t := c.multiply(m)
	return color(t.x, t.y, t.z)
}

// MultiplyColors performs multiplication with tuple components of the
// provided colors and returns the product (the "Hadamard" or "Schur" product).
func (c Color) MultiplyColors(o Color) Color {
	t := c.Multiply(o)
	return color(t.x, t.y, t.z)
}

// Convenience function for instantiating a color
func color(r, g, b float64) Color {
	return Color{
		Tuple: Tuple{r, g, b, 0}}
}

/*
 * Convenience functions to access tuple fields with color names
 */

func (c Color) red() float64 {
	return c.x
}
func (c Color) green() float64 {
	return c.y
}
func (c Color) blue() float64 {
	return c.z
}
