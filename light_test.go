package main

import (
	"math"
	"testing"
)

func TestPointLight(t *testing.T) {
	position := point(0, 0, 0)
	intensity := color(1, 1, 1)
	light := PointLight{position, intensity}
	if !light.position.Equals(position) {
		t.Errorf("Expected position to be %+v, got %+v\n", position, light.position)
	}
	if !light.intensity.Equals(intensity) {
		t.Errorf("Expected intensity to be %+v, got %+v\n", intensity, light.intensity)
	}
}

func TestMaterial(t *testing.T) {
	// A sphere has a default material
	s := sphere()
	if s.material.ambient != 0.1 {
		t.Errorf("Expected default material ambient value to be 0.1, got %f\n", s.material.ambient)
	}
	if s.material.diffuse != 0.9 {
		t.Errorf("Expected default material diffuse value to be 0.9, got %f\n", s.material.diffuse)
	}
	if s.material.specular != 0.9 {
		t.Errorf("Expected default material specular value to be 0.9, got %f\n", s.material.specular)
	}
	if s.material.shininess != 200.0 {
		t.Errorf("Expected default material shininess value to be 0.9, got %f\n", s.material.shininess)
	}

	// A sphere may be assigned a material
	s = sphere()
	m := defaultMaterial()
	m.ambient = 1
	s.material = m
	if s.material.ambient != 1 {
		t.Errorf("Expected material ambient value to be 1, got %f\n", s.material.ambient)
	}
	if !s.material.color.Equals(color(1, 1, 1)) {
		t.Errorf("Expected material color to be 1, 1, 1, got %f\n", s.material.color)
	}
}

func TestLighting(t *testing.T) {
	material := defaultMaterial()
	position := point(0, 0, 0)

	// Lighting with the eye between the light and the surface
	eyeVector := vector(0, 0, -1)
	normalVector := vector(0, 0, -1)
	light := PointLight{point(0, 0, -10), color(1, 1, 1)}

	result := lighting(material, light, position, eyeVector, normalVector)
	expected := color(1.9, 1.9, 1.9)
	if !result.Equals(expected) {
		t.Errorf("Expected lighting result to be %+v, got %+v\n", expected, result)
	}

	// Lighting with the eye between the light and the surface, eye offset 45 degrees
	eyeVector = vector(0, math.Sqrt(2)/2, math.Sqrt(2)/2)
	normalVector = vector(0, 0, -1)
	light = PointLight{point(0, 0, -10), color(1, 1, 1)}
	result = lighting(material, light, position, eyeVector, normalVector)
	expected = color(1.0, 1.0, 1.0)
	if !result.Equals(expected) {
		t.Errorf("Expected lighting result to be %+v, got %+v\n", expected, result)
	}

	// Lighting with the eye opposite the surface, light offset 45 degrees
	eyeVector = vector(0, 0, -1)
	normalVector = vector(0, 0, -1)
	light = PointLight{point(0, 10, -10), color(1, 1, 1)}
	result = lighting(material, light, position, eyeVector, normalVector)
	expected = color(0.7364, 0.7364, 0.7364)
	if !result.Equals(expected) {
		t.Errorf("Expected lighting result to be %+v, got %+v\n", expected, result)
	}

	// Lighting with eye in the path of the reflection vector
	eyeVector = vector(0, -math.Sqrt(2)/2, -math.Sqrt(2)/2)
	normalVector = vector(0, 0, -1)
	light = PointLight{point(0, 10, -10), color(1, 1, 1)}
	result = lighting(material, light, position, eyeVector, normalVector)
	expected = color(1.6364, 1.6364, 1.6364)
	if !result.Equals(expected) {
		t.Errorf("Expected lighting result to be %+v, got %+v\n", expected, result)
	}

	// Light behind the surface
	eyeVector = vector(0, 0, -1)
	normalVector = vector(0, 0, -1)
	light = PointLight{point(0, 0, 10), color(1, 1, 1)}
	result = lighting(material, light, position, eyeVector, normalVector)
	expected = color(0.1, 0.1, 0.1)
	if !result.Equals(expected) {
		t.Errorf("Expected lighting result to be %+v, got %+v\n", expected, result)
	}
}

func TestCastRaysAtSphere(t *testing.T) {
	canvasSize := 50
	canvas := NewCanvas(canvasSize, canvasSize)

	// We assume a unit sphere at the origin
	shape := sphere()
	shape.material.color = color(1, 0.2, 1)

	// White light behind, above, and to the left of the eye
	light := PointLight{
		position: point(-10, 0, -5),
		// position: point(-10, 10, -10),
		intensity: color(1, 1, 1),
	}

	// The wall is 7x7 in size and 10 units behind the sphere
	wallZ := 10.0
	wallSize := 7.0
	half := wallSize / 2
	// A single pixel is this many world space units
	pixelSize := wallSize / float64(canvasSize)

	x, y := 25, 25
	// compute the world y coordinate
	worldY := half - (pixelSize * float64(y))
	// compute the world x coordinate
	worldX := -half + (pixelSize * float64(x))
	// location of the eye/camera
	eye := point(0, 0, 0)
	// the point on the wall that the ray will target
	target := point(worldX, worldY, wallZ)

	// cast a ray from the eye to the target on the wall
	ray := ray(eye, target.Subtract(eye).normalize())
	// ray := ray(light.position, position.Subtract(light.position).normalize())
	intersection, hit := hit(ray.intersect(shape))
	if !hit {
		t.Errorf("Expecting a hit when casting a ray from %+v to %+v\n", ray, target)
	} else {
		point := ray.position(intersection.t)
		// surface normal: perpendicular to the surface at the intersection
		normal := intersection.object.normalAt(point)
		// eye vector: from the intersection back to the source of the ray
		eye := ray.direction.negate()
		// calculate the color at this location
		c := lighting(intersection.object.material, light, point, eye, normal)
		canvas.SetColor(x, y, c)
		if c.Equals(color(0, 0, 0)) {
			t.Error("Expected lighting result to be non-black in the center of the canvas")
		}
	}
}

func TestMaterialEquals(t *testing.T) {
	m1 := Material{}
	m2 := Material{}
	if !m1.Equals(m2) {
		t.Error("Expected materials to be equal")
	}

	m2.color = color(0.123, 0.456, 0.789)
	if m1.Equals(m2) {
		t.Error("Expected materials to be unequal")
	}

	m2 = Material{}
	m2.ambient = 0.123
	if m1.Equals(m2) {
		t.Error("Expected materials to be unequal")
	}

	m2 = Material{}
	m2.diffuse = 0.123
	if m1.Equals(m2) {
		t.Error("Expected materials to be unequal")
	}

	m2 = Material{}
	m2.specular = 0.123
	if m1.Equals(m2) {
		t.Error("Expected materials to be unequal")
	}

	m2 = Material{}
	m2.shininess = 0.123
	if m1.Equals(m2) {
		t.Error("Expected materials to be unequal")
	}
}
