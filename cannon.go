package main

import "fmt"

// Projectile is an object with a position and a velocity
type Projectile struct {
	position Tuple // point
	velocity Tuple // vector
}

// Environment is the world in which projectiles exist
type Environment struct {
	gravity Tuple // vector
	wind    Tuple // vector
}

func fireCannon() {
	proj := Projectile{
		position: point(0, 1, 0),
		velocity: vector(1, 1.8, 0).normalize().multiply(11.25)}
	env := Environment{
		gravity: vector(0, -0.1, 0),
		wind:    vector(-0.01, 0, 0)}

	height := 550
	canvas := NewCanvas(900, height)
	red := color(1, 0, 0)

	for ticks := 1; proj.position.y > 0; ticks++ {
		proj = tick(env, proj)
		canvas.SetColor(int(proj.position.x), height-int(proj.position.y), red)
		fmt.Printf("Tick %d, position: %f\n", ticks, proj.position.y)
	}

	canvas.Save("./cannon.ppm")
}

func tick(env Environment, proj Projectile) Projectile {
	return Projectile{
		position: proj.position.Add(&proj.velocity),
		velocity: proj.velocity.Add(&env.gravity).Add(&env.wind)}
}
