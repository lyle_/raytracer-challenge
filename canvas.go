package main

import (
	"fmt"
	"io/ioutil"
	"math"
	"strconv"
	"strings"
)

// Canvas is a two-dimensional grid of pixels
type Canvas struct {
	width, height int
	pixels        [][]Color
}

// NewCanvas returns a newly-initialized struct of the given dimensions.
func NewCanvas(width, height int) Canvas {
	pixels := make([][]Color, width)
	for i := range pixels {
		pixels[i] = make([]Color, height)
	}
	return Canvas{width: width,
		height: height,
		pixels: pixels}
}

// Color returns the color of the canvas at the designated coordinates.
func (c Canvas) Color(x, y int) Color {
	return c.pixels[x][y]
}

// SetColor writes the provided color to the designated coordinates.
func (c Canvas) SetColor(x, y int, color Color) {
	if x >= 0 && x < c.width && y >= 0 && y < c.height {
		c.pixels[x][y] = color
	}
}

// SetColorAtPoint writes the provided color to the designated coordinates.
func (c Canvas) SetColorAtPoint(point Tupler, color Color) {
	x := int(point.X())
	y := int(point.Y())
	if x >= 0 && x < c.width && y >= 0 && y < c.height {
		c.pixels[x][y] = color
	}
}

// ToPPMString transforms the Canvas into a PPM string.
func (c Canvas) ToPPMString() string {
	var sb strings.Builder
	// Write the header
	fmt.Fprintf(&sb, "P3\n%d %d\n255\n", c.width, c.height)
	// Write each row of pixels to the string
	lineLength := 0
	for y := 0; y < c.height; y++ {
		for x := 0; x < c.width; x++ {
			r, g, b := quantizeColor256(c.Color(x, y))
			writePPMValue(&sb, r, &lineLength)
			writePPMValue(&sb, g, &lineLength)
			writePPMValue(&sb, b, &lineLength)
		}
		sb.WriteString("\n")
		lineLength = 0
	}
	return sb.String()
}

// Save writes the image to the provided filename
func (c Canvas) Save(filename string) {
	err := ioutil.WriteFile(filename, []byte(c.ToPPMString()), 0644)
	if err != nil {
		panic(err)
	}
}

func writePPMValue(sb *strings.Builder, value string, lineLength *int) {
	padding := ""
	if *lineLength > 0 {
		// If we're not at the beginning of the line, pad with a space
		padding = " "
	}

	// If the value would cause the line to go over 70 chars, start a new line
	if *lineLength+len(padding+value) >= 70 {
		sb.WriteString("\n")
		padding = ""
		*lineLength = 0
	}
	sb.WriteString(padding + value)
	*lineLength += len(padding + value)
}

func quantizeColor256(c Color) (string, string, string) {
	return strconv.Itoa(quantizeFloatToInt(c.red(), 255)),
		strconv.Itoa(quantizeFloatToInt(c.green(), 255)),
		strconv.Itoa(quantizeFloatToInt(c.blue(), 255))
}

// Expects a floating-point number between 0 and 1 inclusive
func quantizeFloatToInt(f float64, max int) int {
	result := int(math.Ceil(f * float64(max)))
	if result < 0 {
		return 0
	} else if result > max {
		return max
	} else {
		return result
	}
}
