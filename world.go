package main

import "sort"

// World is a collection of all objects in a scene.
type World struct {
	objects []Sphere
	lights  []PointLight
}

// NewDefaultWorld returns a newly-created world.
func NewDefaultWorld() World {
	sphere1 := sphere()
	sphere1.material = Material{
		color:    color(0.8, 1.0, 0.6),
		diffuse:  0.7,
		specular: 0.2,
	}

	sphere2 := sphere()
	sphere2.transform = scaling(0.5, 0.5, 0.5)

	return World{
		lights: []PointLight{
			PointLight{
				position:  point(-10, 10, -10),
				intensity: color(1, 1, 1),
			},
		},
		objects: []Sphere{sphere1, sphere2},
	}
}

// Naive implementation, O(n)
func (w World) containsObject(object Sphere) bool {
	for _, o := range w.objects {
		if o.equals(object) {
			return true
		}
	}
	return false
}

func (w World) intersect(ray Ray) []Intersection {
	var results []Intersection
	// Check all objects in the world for intersections with the ray
	for _, object := range w.objects {
		objectIntersections := ray.intersect(object)
		results = append(results, objectIntersections...)
	}

	// Sort by t value, ascending
	sort.Slice(results, func(i, j int) bool {
		return results[i].t < results[j].t
	})

	return results
}
