package main

import (
	"strings"
	"testing"
)

var black Color = color(0, 0, 0)
var red Color = color(1, 0, 0)

func TestCanvasInitializer(t *testing.T) {
	// Create a canvas with the initializer function
	c := NewCanvas(10, 20)
	if c.width != 10 || c.height != 20 {
		t.Error("Canvas expected to be 10x20, got ", c)
	}
	if len(c.pixels) != c.width {
		t.Error("Canvas width expected to be 10, got ", len(c.pixels))
	}
	if len(c.pixels[0]) != c.height {
		t.Error("Canvas height expected to be 20, got ", len(c.pixels[0]))
	}
	for x, row := range c.pixels {
		for y, pixel := range row {
			if !pixel.Equals(black) {
				t.Errorf("Expected pixels to be initialized to black; got %+v at %dx%d", pixel, x, y)
			}
		}
	}
}

func TestCanvasPixelRead(t *testing.T) {
	c := NewCanvas(10, 20)
	if !c.Color(1, 10).Equals(black) {
		t.Error("Expected to read the default black pixel, got", c.Color(1, 10))
	}
}

func TestCanvasPixelSet(t *testing.T) {
	c := NewCanvas(10, 20)
	c.SetColor(1, 10, red)
	if !c.Color(1, 9).Equals(black) {
		t.Error("Expected to read the default black pixel, got", c.Color(1, 9))
	}
	if !c.Color(1, 10).Equals(red) {
		t.Error("Expected to read the overwritten red pixel, got", c.Color(1, 10))
	}
}

func TestCanvasPixelSetWithTuple(t *testing.T) {
	c := NewCanvas(10, 20)
	point := point(1, 10, 0)
	c.SetColorAtPoint(point, red)
	if !c.Color(1, 9).Equals(black) {
		t.Error("Expected to read the default black pixel, got", c.Color(1, 9))
	}
	if !c.Color(1, 10).Equals(red) {
		t.Error("Expected to read the overwritten red pixel, got", c.Color(1, 10))
	}
}

func TestPPMToStringHeader(t *testing.T) {
	c := NewCanvas(5, 3)
	ppmLines := strings.Split(c.ToPPMString(), "\n")
	if ppmLines[0] != "P3" || ppmLines[1] != "5 3" || ppmLines[2] != "255" {
		t.Error("Expecting a valid PPM header, got", ppmLines)
	}
}

func TestQuantizeFloatToInt(t *testing.T) {
	max := 255
	quantized := quantizeFloatToInt(-0.5, max)
	if quantized != 0 {
		t.Error("Quantized values less than 0 should be clamped", quantized)
	}

	quantized = quantizeFloatToInt(1.5, max)
	if quantized != 255 {
		t.Errorf("Quantized values greater than than %d should be clamped, got %d", max, quantized)
	}

	quantized = quantizeFloatToInt(.5, max)
	if quantized != 128 {
		t.Error("Quantized value expected to be 128, got", quantized)
	}
}

func TestQuantizeColor(t *testing.T) {
	r, g, b := quantizeColor256(color(0, .5, 1))
	if r != "0" && g != string(256/2) && b != "255" {
		t.Errorf("Quantized color expected to be {0, 128, 255}, got {%s %s %s}", r, g, b)
	}

	r, g, b = quantizeColor256(color(1.5, 0, 0))
	if r != "255" {
		t.Error("Expected clamped value of 255, got ", r)
	}
}

func TestPPMToStringPixels(t *testing.T) {
	c := NewCanvas(5, 3)
	c.SetColor(0, 0, color(1.5, 0, 0))
	c.SetColor(2, 1, color(0, 0.5, 0))
	c.SetColor(4, 2, color(-.5, 0, 1))
	lines := strings.Split(c.ToPPMString(), "\n")
	if len(lines) != 7 {
		// Expecting 3 header lines, 3 for pixel rows, and trailing newline
		t.Error("A 5x3 canvas should produce a 7-line PPM file, got", len(lines))
	}

	payload := strings.Join(lines[3:], "\n")
	expected :=
		`255 0 0 0 0 0 0 0 0 0 0 0 0 0 0
0 0 0 0 0 0 0 128 0 0 0 0 0 0 0
0 0 0 0 0 0 0 0 0 0 0 0 0 0 255
`
	if payload != expected {
		t.Errorf("Unexpected pixel data.\nExpected:\n%s\nGot:\n%s\n", expected, payload)
	}
}

func TestPPMLineLength(t *testing.T) {
	c := NewCanvas(10, 2)
	for y := 0; y < c.height; y++ {
		for x := 0; x < c.width; x++ {
			c.SetColor(x, y, color(1, .8, .6))
		}
	}
	lines := strings.Split(c.ToPPMString(), "\n")
	payload := strings.Join(lines[3:], "\n")
	expected :=
		`255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204
153 255 204 153 255 204 153 255 204 153 255 204 153
255 204 153 255 204 153 255 204 153 255 204 153 255 204 153 255 204
153 255 204 153 255 204 153 255 204 153 255 204 153
`
	if payload != expected {
		t.Errorf("Pixel data lines should not exceed 70 chars.\nExpected:\n%s\nGot:\n%s\n", expected, payload)
	}
}

func TestOutOfBounds(t *testing.T) {
	c := NewCanvas(10, 10)
	// Do a bunch of writes to out-of-bounds coordinates
	c.SetColor(-1, 0, red)
	c.SetColor(0, -1, red)
	c.SetColor(-1, -1, red)
	c.SetColor(10, 0, red)
	c.SetColor(0, 10, red)

	// Verify that none of those worked
	for x, row := range c.pixels {
		for y, pixel := range row {
			if !pixel.Equals(black) {
				t.Errorf("Not expecting any color changes to have occurred; got %+v at %dx%d", pixel, x, y)
			}
		}
	}
}
