package main

import (
	// "fmt"
	"math"
	// "strconv"
)

// Matrix is a grid of numbers which can be manipulated as a single unit.
type Matrix struct {
	width, height int
	elements      [][]float64
}

// NewMatrix returns a newly-initialized matrix of the given dimensions.
func NewMatrix(width, height int) Matrix {
	elements := make([][]float64, width)
	for i := range elements {
		elements[i] = make([]float64, height)
	}
	return Matrix{width: width,
		height:   height,
		elements: elements}
}

// At returns the value at the given coordinates.
func (m Matrix) At(row, col int) float64 {
	return m.elements[row][col]
}

// Set writes the given value into the specified coordinates.
func (m Matrix) Set(row, col int, value float64) {
	m.elements[row][col] = value
}

// SetRow writes the given values into the specified row.
func (m Matrix) SetRow(row int, value []float64) {
	for i := 0; i < len(value); i++ {
		m.elements[row][i] = value[i]
	}
}

// Load stores the given values in the matrix.
func (m Matrix) Load(data [][]float64) {
	copy(m.elements, data)
}

// Equals returns whether the given matrices are equal.
func (m Matrix) Equals(o Matrix) bool {
	if m.width != o.width || m.height != o.height {
		return false
	}
	for row := 0; row < m.height; row++ {
		for col := 0; col < m.width; col++ {
			if !floatEqual(m.elements[row][col], o.elements[row][col]) {
				return false
			}
		}
	}

	return true
}

func slicesEqual(a, b []float64) bool {
	if (a == nil) != (b == nil) {
		return false
	}

	if len(a) != len(b) {
		return false
	}

	for i := range a {
		if a[i] != b[i] {
			return false
		}
	}
	return true
}

// Computes the determinant of a 2x2 matrix
func (m Matrix) determinant() float64 {
	if m.width == 2 {
		return m.elements[0][0]*m.elements[1][1] -
			m.elements[0][1]*m.elements[1][0]
	}
	determinant := 0.0
	for col := 0; col < m.width; col++ {
		determinant += m.elements[0][col] * m.cofactor(0, col)
	}
	return determinant
}

// The minor of element [i,j] is the determinant of the submatrix at [i,j]
func (m Matrix) minor(row, column int) float64 {
	return m.Submatrix(row, column).determinant()
}

// Cofactor is a minor which may have its sign changed depending on its location
func (m Matrix) cofactor(row, column int) float64 {
	cofactor := m.minor(row, column)
	if (row+column)%2 != 0 {
		// If row + column is odd, negate the minor
		cofactor = -cofactor
	}
	return cofactor
}

func (m Matrix) invertible() bool {
	return m.determinant() != 0
}

func (m Matrix) inverse() Matrix {
	if !m.invertible() {
		panic("Matrix is not invertible")
	}

	determinant := m.determinant()
	result := NewMatrix(m.width, m.height)
	for row := 0; row < result.height; row++ {
		for column := 0; column < result.height; column++ {
			// swapping row, column here accomplishes the transpose operation
			result.elements[column][row] = m.cofactor(row, column) / determinant
		}
	}
	return result
}

func translation(x, y, z float64) Matrix {
	// Create an identity matrix with the x, y, z values added
	matrix := NewMatrix(4, 4)
	matrix.Load([][]float64{
		{1, 0, 0, x},
		{0, 1, 0, y},
		{0, 0, 1, z},
		{0, 0, 0, 1},
	})
	return matrix
}

func scaling(x, y, z float64) Matrix {
	// Create an identity matrix with the x, y, z values added
	matrix := NewMatrix(4, 4)
	matrix.Load([][]float64{
		{x, 0, 0, 0},
		{0, y, 0, 0},
		{0, 0, z, 0},
		{0, 0, 0, 1},
	})
	return matrix
}

func rotationX(radians float64) Matrix {
	matrix := NewMatrix(4, 4)
	matrix.Load([][]float64{
		{1, 0, 0, 0},
		{0, math.Cos(radians), -math.Sin(radians), 0},
		{0, math.Sin(radians), math.Cos(radians), 0},
		{0, 0, 0, 1},
	})
	return matrix
}

func rotationY(radians float64) Matrix {
	matrix := NewMatrix(4, 4)
	matrix.Load([][]float64{
		{math.Cos(radians), 0, 0, math.Sin(radians)},
		{0, 1, 0, 0},
		{-math.Sin(radians), 0, math.Cos(radians), 0},
		{0, 0, 0, 1},
	})
	return matrix
}

func rotationZ(radians float64) Matrix {
	matrix := NewMatrix(4, 4)
	matrix.Load([][]float64{
		{math.Cos(radians), -math.Sin(radians), 0, 0},
		{math.Sin(radians), math.Cos(radians), 0, 0},
		{0, 0, 1, 0},
		{0, 0, 0, 1},
	})
	return matrix
}

func shearing(xy, xz, yx, yz, zx, zy float64) Matrix {
	matrix := NewMatrix(4, 4)
	matrix.Load([][]float64{
		{1, xy, xz, 0},
		{yx, 1, yz, 0},
		{zx, zy, 1, 0},
		{0, 0, 0, 1},
	})
	return matrix
}

// Row returns the given row.
func (m Matrix) Row(row int) []float64 {
	return m.elements[row]
}

// RowTuple returns the given row as a tuple.
func (m Matrix) RowTuple(row int) Tuple {
	return Tuple{m.elements[row][0],
		m.elements[row][1],
		m.elements[row][2],
		m.elements[row][3],
	}
}

// Column returns the given column.
func (m Matrix) Column(column int) []float64 {
	result := make([]float64, m.height)
	for row := 0; row < m.height; row++ {
		result[row] = m.elements[row][column]
	}
	return result
}

// Multiply computes the product of the two matrices and returns the result.
func (m Matrix) Multiply(o Matrix) Matrix {
	result := NewMatrix(m.width, m.height)
	for rowNum := 0; rowNum < m.height; rowNum++ {
		for colNum := 0; colNum < m.width; colNum++ {
			row := m.Row(rowNum)
			col := o.Column(colNum)
			for i := 0; i < len(row); i++ {
				result.elements[rowNum][colNum] += row[i] * col[i]
			}
		}
	}

	return result
}

// MultiplyTupler computes the product of the matrix with the given tuple.
func (m Matrix) MultiplyTupler(t Tupler) Tuple {
	return Tuple{
		m.RowTuple(0).Dot(t),
		m.RowTuple(1).Dot(t),
		m.RowTuple(2).Dot(t),
		m.RowTuple(3).Dot(t),
	}
}

// Transpose performs matrix transposition and returns the result
func (m Matrix) Transpose() Matrix {
	result := NewMatrix(m.width, m.height)
	for rowNum := 0; rowNum < m.height; rowNum++ {
		result.SetRow(rowNum, m.Column(rowNum))
	}
	return result
}

// Submatrix returns a copy of the matrix with the given row and column removed
func (m Matrix) Submatrix(removeRow, removeCol int) Matrix {
	result := NewMatrix(m.width-1, m.height-1)
	var targetRow, targetColumn int
	for row := 0; row < m.height; row++ {
		if row == removeRow {
			continue // skip this row
		}
		for col := 0; col < m.width; col++ {
			if col != removeCol {
				result.elements[targetRow][targetColumn] = m.elements[row][col]
				targetColumn++
			}
		}
		targetRow++
		targetColumn = 0
	}
	return result
}
