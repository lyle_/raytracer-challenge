package main

import "testing"

func TestWorldCreation(t *testing.T) {
	w := World{}
	if len(w.objects) > 0 {
		t.Error("Expected objects to be empty")
	}
	if len(w.lights) > 0 {
		t.Error("Expected lights to be empty")
	}
}

func TestDefaultWorldCreation(t *testing.T) {
	w := NewDefaultWorld()

	if len(w.objects) != 2 {
		t.Errorf("Expected two spheres in a default world, got %d\n", len(w.objects))
	}
	expectedSphere1 := sphere()
	expectedSphere1.material = Material{
		color:    color(0.8, 1.0, 0.6),
		diffuse:  0.7,
		specular: 0.2,
	}
	expectedSphere2 := sphere()
	expectedSphere2.transform = scaling(0.5, 0.5, 0.5)
	if !w.containsObject(expectedSphere1) {
		t.Errorf("Expected world to contain %+v\n", expectedSphere1)
	}
	if !w.containsObject(expectedSphere2) {
		t.Errorf("Expected world to contain %+v\n", expectedSphere2)
	}

	if len(w.lights) != 1 {
		t.Errorf("Expected one light in a default world, got %d\n", len(w.lights))
	}
	expectedDefaultLight := PointLight{
		position:  point(-10, 10, -10),
		intensity: color(1, 1, 1),
	}
	if w.lights[0] != expectedDefaultLight {
		t.Errorf("Expected default light of %+v, got %+v\n", expectedDefaultLight, w.lights[0])
	}
}

func TestIntersectWorld(t *testing.T) {
	w := NewDefaultWorld()
	r := ray(point(0, 0, -5), vector(0, 0, 1))

	xs := w.intersect(r)
	if len(xs) != 4 {
		t.Errorf("Expected four intersections, got %d", len(xs))
	}
	if xs[0].t != 4 {
		t.Error("Expected ray/sphere intersection not found")
	}
	if xs[1].t != 4.5 {
		t.Error("Expected ray/sphere intersection not found")
	}
	if xs[2].t != 5.5 {
		t.Error("Expected ray/sphere intersection not found")
	}
	if xs[3].t != 6 {
		t.Error("Expected ray/sphere intersection not found")
	}
}
