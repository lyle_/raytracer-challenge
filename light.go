package main

import (
	"math"
)

// PointLight is a light source with no size, existing at a single point in space.
type PointLight struct {
	position  Tuple
	intensity Color
}

// Material describes the optical attributes of an object for the Phong reflection model.
type Material struct {
	color                                 Color
	ambient, diffuse, specular, shininess float64
}

// Equals returns whether the receiving material is the same as the argument.
func (m Material) Equals(o Material) bool {
	return m.color == o.color &&
		m.ambient == o.ambient &&
		m.diffuse == o.diffuse &&
		m.specular == o.specular &&
		m.shininess == o.shininess
}

func defaultMaterial() Material {
	return Material{
		color:     color(1, 1, 1),
		ambient:   0.1,
		diffuse:   0.9,
		specular:  0.9,
		shininess: 200.0,
	}
}

func lighting(material Material, light PointLight, targetPoint, eyeVector, normalVector Tuple) Color {
	var ambient, specular, diffuse Color
	black := color(0, 0, 0)

	// Combine the surface color with the light's color/intensity
	effectiveColor := material.color.MultiplyColors(light.intensity)

	// Compute the ambient contribution
	ambient = effectiveColor.MultiplyColor(material.ambient)

	// Find the direction to the light source from the target point
	lightVector := light.position.Subtract(targetPoint).normalize()

	// Represents the cosine of the angle between the light and normal vectors.
	lightDotNormal := lightVector.Dot(normalVector)
	if lightDotNormal < 0 {
		// A negative number means the light is on the other side of the surface.
		diffuse = black
		specular = black
	} else {
		diffuse = effectiveColor.MultiplyColor(material.diffuse * lightDotNormal)
	}

	reflectVector := lightVector.negate().reflect(normalVector)
	// Represents the cosine of the angle between the light and normal vectors.
	reflectDotEye := reflectVector.Dot(eyeVector)
	if reflectDotEye < 0 {
		// A negative number means the light reflects away from the eye
		specular = black
	} else {
		factor := math.Pow(reflectDotEye, material.shininess)
		specular = light.intensity.MultiplyColor(material.specular).MultiplyColor(factor)
	}

	// Add the three contributions together to get the final shading
	color := ambient.AddColor(diffuse).AddColor(specular)
	return color
}
