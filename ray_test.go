package main

import (
	"testing"
)

func TestRayCreationAndQuery(t *testing.T) {
	origin := point(1, 2, 3)
	direction := vector(4, 5, 6)
	ray := ray(origin, direction)
	if ray.origin != origin {
		t.Errorf("Expected ray origin %+v to match origin %+v", ray.origin, origin)
	}
	if ray.direction != direction {
		t.Errorf("Expected ray direction %+v to match direction %+v", ray.direction, direction)
	}
}

func TestRayPosition(t *testing.T) {
	ray := ray(point(2, 3, 4), vector(1, 0, 0))

	position := ray.position(0)
	expected := point(2, 3, 4)
	if !position.Equals(expected) {
		t.Errorf("Expected ray position %+v to be %+v", position, expected)
	}

	position = ray.position(1)
	expected = point(3, 3, 4)
	if !position.Equals(expected) {
		t.Errorf("Expected ray position %+v to be %+v", position, expected)
	}

	position = ray.position(-1)
	expected = point(1, 3, 4)
	if !position.Equals(expected) {
		t.Errorf("Expected ray position %+v to be %+v", position, expected)
	}

	position = ray.position(2.5)
	expected = point(4.5, 3, 4)
	if !position.Equals(expected) {
		t.Errorf("Expected ray position %+v to be %+v", position, expected)
	}
}

func TestRaySphereIntersection(t *testing.T) {
	// A ray intersects a sphere at two points
	ray := ray(point(0, 0, -5), vector(0, 0, 1))
	sphere := sphere()
	xs := ray.intersect(sphere)
	if len(xs) != 2 {
		t.Error("Expected two intersection points, got", len(xs))
	}
	if xs[0].t != 4.0 {
		t.Error("Expected xs[0].t to be 4.0, got", xs[0])
	}
	if xs[1].t != 6.0 {
		t.Error("Expected xs[1].t to be 6.0, got", xs[1])
	}
}

func TestRaySphereTangent(t *testing.T) {
	// A ray intersects a sphere at a tangent
	ray := ray(point(0, 1, -5), vector(0, 0, 1))
	sphere := sphere()
	xs := ray.intersect(sphere)
	if len(xs) != 2 {
		t.Error("Expected two intersection points, got", len(xs))
	}
	if xs[0].t != 5.0 {
		t.Error("Expected xs[0].t to be 5.0, got", xs[0])
	}
	if xs[1].t != 5.0 {
		t.Error("Expected xs[1].t to be 5.0, got", xs[1])
	}
}

func TestRayMissesSphere(t *testing.T) {
	// A ray misses a sphere
	ray := ray(point(0, 2, -5), vector(0, 0, 1))
	sphere := sphere()
	xs := ray.intersect(sphere)
	if len(xs) != 0 {
		t.Error("Expected no intersection points, got", len(xs))
	}
}

func TestRayOriginatesInSphere(t *testing.T) {
	ray := ray(point(0, 0, 0), vector(0, 0, 1))
	sphere := sphere()
	xs := ray.intersect(sphere)
	if len(xs) != 2 {
		t.Error("Expected 2 intersection points, got", len(xs))
	}
	if xs[0].t != -1.0 {
		t.Error("Expected xs[0].t to be -1.0, got", xs[0])
	}
	if xs[1].t != 1.0 {
		t.Error("Expected xs[1].t to be 1.0, got", xs[1])
	}
}

func TestSphereBehindRay(t *testing.T) {
	// A sphere completely behind a ray's starting point is still intersected
	ray := ray(point(0, 0, 5), vector(0, 0, 1))
	sphere := sphere()
	xs := ray.intersect(sphere)
	if len(xs) != 2 {
		t.Error("Expected 2 intersection points, got", len(xs))
	}
	if xs[0].t != -6.0 {
		t.Error("Expected xs[0].t to be -6.0, got", xs[0])
	}
	if xs[1].t != -4.0 {
		t.Error("Expected xs[1].t to be -4.0, got", xs[1])
	}
}

func TestRayTranslation(t *testing.T) {
	r := ray(point(1, 2, 3), vector(0, 1, 0))
	m := translation(3, 4, 5)
	r2 := r.transform(m)
	expectedOrigin := point(4, 6, 8)
	if !r2.origin.Equals(expectedOrigin) {
		t.Errorf("Expected r2.origin to be %+v, got %+v\n", expectedOrigin, r2.origin)
	}
	expectedDirection := vector(0, 1, 0)
	if !r2.direction.Equals(expectedDirection) {
		t.Errorf("Expected r2.direction to be %+v, got %+v\n", expectedDirection, r2.direction)
	}
}

func TestRayScaling(t *testing.T) {
	r := ray(point(1, 2, 3), vector(0, 1, 0))
	m := scaling(2, 3, 4)
	r2 := r.transform(m)
	expectedOrigin := point(2, 6, 12)
	if !r2.origin.Equals(expectedOrigin) {
		t.Errorf("Expected r2.origin to be %+v, got %+v\n", expectedOrigin, r2.origin)
	}
	expectedDirection := vector(0, 3, 0)
	if !r2.direction.Equals(expectedDirection) {
		t.Errorf("Expected r2.direction to be %+v, got %+v\n", expectedDirection, r2.direction)
	}
}
