package main

import (
	"math"
)

// Tupler is a thing that can perform Tuple operations.
type Tupler interface {
	IsPoint() bool
	IsVector() bool
	Equals(Tupler) bool
	Add(Tupler) Tuple
	Subtract(Tupler) Tuple
	Dot(Tupler) float64
	X() float64
	Y() float64
	Z() float64
	W() float64
}

// Upper bound on floating point errors.
const epsilon = 0.00001

// Tuple is an ordered list of points.
// Points are denoted by a `w` value of 1.
// Vectors are denoted by a `w` value of 0.
type Tuple struct {
	x, y, z, w float64
}

// X is the x value of the tuple
func (t Tuple) X() float64 {
	return t.x
}

// Y is the y value of the tuple
func (t Tuple) Y() float64 {
	return t.y
}

// Z is the z value of the tuple
func (t Tuple) Z() float64 {
	return t.z
}

// W is the w value of the tuple; 1 if it represents a point, 0 for a vector.
func (t Tuple) W() float64 {
	return t.w
}

// IsPoint returns whether the tuple is a single point as opposed to a vector.
func (t Tuple) IsPoint() bool {
	if t.w == 1.0 {
		return true
	}
	return false
}

// IsVector returns whether the tuple is a vector as opposed to a single point.
func (t Tuple) IsVector() bool {
	if t.w == 0.0 {
		return true
	}
	return false
}

// Equals returns whether the given tuples are equal.
func (t Tuple) Equals(o Tupler) bool {
	return floatEqual(t.x, o.X()) &&
		floatEqual(t.y, o.Y()) &&
		floatEqual(t.z, o.Z()) &&
		floatEqual(t.w, o.W())
}

// Add performs addition on the two tuples and returns the result.
func (t Tuple) Add(o Tupler) Tuple {
	return Tuple{t.x + o.X(),
		t.y + o.Y(),
		t.z + o.Z(),
		t.w + o.W()}
}

// Subtract performs subraction on the two tuples and returns the difference.
func (t Tuple) Subtract(o Tupler) Tuple {
	return Tuple{t.x - o.X(),
		t.y - o.Y(),
		t.z - o.Z(),
		t.w - o.W()}
}

// Multiply returns the product of two tuples.
func (t Tuple) Multiply(o Tupler) Tuple {
	return Tuple{t.x * o.X(),
		t.y * o.Y(),
		t.z * o.Z(),
		t.w * o.W()}
}

// Dot returns the dot product of this and the provided tupler.
func (t Tuple) Dot(o Tupler) float64 {
	return t.x*o.X() +
		t.y*o.Y() +
		t.z*o.Z() +
		t.w*o.W()
}

func point(x, y, z float64) Tuple {
	return Tuple{x, y, z, 1}
}

func vector(x, y, z float64) Tuple {
	return Tuple{x, y, z, 0}
}

func floatEqual(a, b float64) bool {
	return math.Abs(a-b) < epsilon
}

func (t Tuple) negate() Tuple {
	return Tuple{-t.x, -t.y, -t.z, -t.w}
}

func (t Tuple) multiply(m float64) Tuple {
	return Tuple{t.x * m,
		t.y * m,
		t.z * m,
		t.w * m}
}

func (t Tuple) divideBy(d float64) Tuple {
	return Tuple{t.x / d,
		t.y / d,
		t.z / d,
		t.w / d}
}

func (t Tuple) magnitude() float64 {
	return math.Sqrt((t.x * t.x) + (t.y * t.y) + (t.z * t.z) + (t.w * t.w))
}

func (t Tuple) normalize() Tuple {
	mag := t.magnitude()
	return Tuple{t.x / mag,
		t.y / mag,
		t.z / mag,
		t.w / mag}
}

// cross products only apply to vector tuples
func (t Tuple) cross(o Tuple) Tuple {
	return Tuple{t.y*o.z - t.z*o.y,
		t.z*o.x - t.x*o.z,
		t.x*o.y - t.y*o.x,
		0}
}

func (t Tuple) reflect(normal Tuple) Tuple {
	return t.Subtract(normal.multiply(t.Dot(normal)).multiply(2))
}
